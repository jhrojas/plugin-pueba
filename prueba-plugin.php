<?php



/*
 * Plugin name: Plugin-prueba
 * Plugin URI: http://ticsmart.com/
 * Description: Plugin para probar actualizacion automatica
 * Version: 1.2
 * Author: Ticsmart SL
 * Author URI: http://ticsmart.com/
 * License: GPL2
 */

require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://bitbucket.org/jhrojas/plugin-pueba/',
	__FILE__,
	'Plugin-prueba'
);

//Opcional: se establece la rama que contiene la versión estable.
$myUpdateChecker->setBranch('master');
?>